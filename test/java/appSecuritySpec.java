package app;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Tag;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Security unit tests")
@Tag("security")
public class appSecuritySpec {

    @Test
    public void BiggerThanIntMaxSizeNeedsApproval() {
        Main app = new Main();
        Amount amount = new Amount(2147483647);
        Amount amount2 = new Amount(1);
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            boolean res = app.approval(amount.add(amount2));
        });
    }

    @Test
    public void LessThanIntMinSizeNeedsApproval() {
        Main app = new Main();
        Amount amount = new Amount(-2147483648);
        Amount amount2 = new Amount(-1);
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            boolean res = app.approval(amount.add(amount2));
        });

    }
}
