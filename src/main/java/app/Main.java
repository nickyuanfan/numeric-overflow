package app;

import java.math.BigDecimal;

public class Main {

    private final int threshold = 1000;

    public static void main(String[] args) {

        Main app = new Main();

        Amount amount = new Amount(new BigDecimal(999));
        if (!app.approval(amount)) {
            System.out.println(amount + " does not require approval");
        }
        amount = new Amount(new BigDecimal(2000));
        if (app.approval(amount)) {
            System.out.println(amount + " requires approval");
        }
    }

    public boolean approval(Amount amount) {
        return amount.isAmountGreaterOrEqualto(threshold);
    }

}
