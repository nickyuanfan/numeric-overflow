package app;

import java.math.BigDecimal;

/**
 *   The Add function cannot return an integer or else it will
 *   overflow as a param. Thus we passing in a BigDecimal or an Integer for the Unit Test.
 *   The validation will sit in the constructor because it's existence
 *   means it's validated.
 */
class Amount {

    private final BigDecimal value;

    Amount(Integer value) {
        amountNotExceedMinOrMaxRange(new BigDecimal(value));
        this.value = BigDecimal.valueOf(value);
    }

    Amount(BigDecimal value) {
        amountNotExceedMinOrMaxRange(value);
        this.value = value;
    }

    private void amountNotExceedMinOrMaxRange(BigDecimal value) {
        if (value.compareTo(BigDecimal.valueOf(Integer.MAX_VALUE)) > 0 || value.compareTo(BigDecimal.valueOf(Integer.MIN_VALUE)) < 0) {
            throw new IllegalArgumentException("Exceeded Range");
        }
    }

    boolean isAmountGreaterOrEqualto(Integer threshold) {
        return value.compareTo(BigDecimal.valueOf(threshold)) >= 0;
    }


    public Amount add(Amount amountToAdd) {
        return new Amount(value.add(amountToAdd.value));
    }
}
